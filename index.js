var plugin = require('./lib/index');

module.exports = function(j) {
    return plugin.register(j);
};

module.exports.NodeUtils = require("./lib/util/node-util");
module.exports.strategies = {
    body: require("./lib/strategy/body"),
    factory: require("./lib/strategy/factory")
};
