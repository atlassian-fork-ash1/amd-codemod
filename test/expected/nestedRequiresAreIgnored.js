//noinspection JSUnusedLocalSymbols
define("test/input/nestedRequires", ["require"], function(require) {
    var baz = require("b/a/z");
    var bar = require("b/a/r");
    var foo = require("f/o/o");
    // require() lines should show above this comment.
    return {
        someFunction() {
            foo.bar();
            bar.baz();
            baz.wtf();
            Bazfoo(bar, baz);
        },
        someOtherFunction() {
            require("crap");
        }
    }
});
