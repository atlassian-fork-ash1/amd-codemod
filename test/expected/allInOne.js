define("module/a", ["b/a/r", "f/o/o"], function(bar, foo) {
    var one = foo("one");
    var two = bar.baz("two");
    return one;
});

require(["foo/bar", "module/a", "module/b"], function(foobar, a, b) {
    foobar("loaded!", a, b, somedep);
});

define("module/b", ["require"], function(require) {
    var bar = require("b/a/r");
    var a = require("module/a");
    var b = a + bar;
    return b;
});
