define("a/module", ["require"], function(require) {
    "use strict";
    var depC = require("dep/c");
    var depB = require("dep/b");
    var depA = require("dep/a");
    depA.foo();
    depB(depC);
});
