//noinspection JSUnusedLocalSymbols
define("test/input/requireStrategyPerLine3", ["require"], function(require) {
    // require() lines should show above this comment.
    foo.bar();
    bar.baz();
    baz.wtf();
    bazfoo(bar, baz);

    //noinspection JSUnusedLocalSymbols
    var nestedBody = function() {
        bar.fly();
    };
});
