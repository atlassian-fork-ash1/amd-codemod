define(
    'test/input/funkyFormatting',
    [
        "not/related"
    ], function (
        NotRelated
    ) {
        NotRelated.init();
        foo.run("a random command");

        return foo.results[1];
    }
);
