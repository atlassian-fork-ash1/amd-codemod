define("test/binary-expressions", ["some/dep"], function(somedep) {
    var check1 = $("whatever") instanceof $;
    var check2 = somedep instanceof F.O.O.bar("whatever");
    var check3 = B.A.R.baz === barbaz;
    var check4 = typeof F.O.O.bar === 'function';
});
