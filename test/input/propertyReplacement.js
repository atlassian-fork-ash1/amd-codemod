define("test/property-replacement", ["require"], function(require) {
    var handler = B.A.R.baz;
    Namespace.bind("ready", handler);

    if (B.A.R.foo === true) {
        Namespace.one("click", handler);
    }

    Namespace.trigger("ready");
});
