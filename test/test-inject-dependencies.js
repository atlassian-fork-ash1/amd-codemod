"use strict";
const assertTransformFactory = require('./utils/assert-transform.js');
const cleanup = require('./utils/cleanup.js');
const EventEmitter = require('events');

//jscodeshift throws a warning without the EventEmitter.defaultMaxListeners increase:
//
//  (node) warning: possible EventEmitter memory leak detected. 11 disconnect listeners added. Use emitter.setMaxListeners() to increase limit.
//  Trace
//      at EventEmitter.addListener (events.js:252:17)
//      at /Users/sergio/src/frontend/amd-codemod/node_modules/jscodeshift/dist/Runner.js:227:24
//      at new Promise (/Users/sergio/src/frontend/amd-codemod/node_modules/core-js/library/modules/es6.promise.js:197:7)
//      at /Users/sergio/src/frontend/amd-codemod/node_modules/jscodeshift/dist/Runner.js:226:16
//      at Array.map (native)
//      at /Users/sergio/src/frontend/amd-codemod/node_modules/jscodeshift/dist/Runner.js:207:22
//      at run (/Users/sergio/src/frontend/amd-codemod/node_modules/core-js/library/modules/es6.promise.js:104:47)
//      at /Users/sergio/src/frontend/amd-codemod/node_modules/core-js/library/modules/es6.promise.js:115:28
//      at flush (/Users/sergio/src/frontend/amd-codemod/node_modules/core-js/library/modules/$.microtask.js:19:5)
//      at _combinedTickCallback (node.js:376:9)
//      at process._tickCallback (node.js:407:11)
//
EventEmitter.defaultMaxListeners=0;

describe("Transform: toAMD", function() {
    after(cleanup);

    describe("basic replacements", function() {
        const assertTransform = assertTransformFactory("codemods/inject-modules.js")

        const tests = {
            "should replace a variable with the equivalent AMD module": "variable.js",
            "should not replace a variable when it's an attribute of another object": "variableIsAttributeOfAnObject.js",
            "should replace multiple variables with their AMD module equivalents": "variables.js",
            "should replace a function with the equivalent AMD module": "function.js",
            "should replace multiple functions with their AMD module equivalents": "functions.js",
            "should replace multiple variables and functions with their AMD module equivalents": "variablesAndFunctions.js",
            "should work when a module has no dependencies": "noDependencies.js",
            "should handle nested namespaces": "nestedNamespaces.js",
            "should handle nested namespaces 2": "nestedNamespaces2.js",
            "should handle nested function invocations": "nestedFuncInvocation1.js",
            "should handle nested function invocations 2": "nestedFuncInvocation2.js",
            "should not mix strategies for require()": "requireStrategyPerLine.js",
            "should append require() after last require() in body": "requireStrategyPerLine2.js",
            "should put require() at beginning of body if no other requires() yet": "requireStrategyPerLine3.js",
            "should put require() at beginning of body even if body contains nested requires": "nestedRequiresAreIgnored.js",
            "should put require() after the 'use strict' statement": "requireStrategyWithUseStrict.js",
            "should put require() after the 'use strict' statement without getting confused by other 'use strict'": "requireStrategyWithUseStrict2.js",
            "should handle complex objects such as jQuery": "jqueryVarFunc.js",
            "should handle replacements inside binary expressions": "binaryExpressions.js",
            "should not interfere with an existing dependency with a different varname": "existingDependencyWithDifferentVarname.js",
            "should not interfere with an existing dependency with a different varname when using per-line require()": "existingPerLineDependencyWithDifferentVarname.js",
            "should replace a variable with the equivalent AMD module when no child is invoked": "variableWithoutChildInvocation.js",
            "should replace a function with the equivalent AMD module when it's not called": "functionWithoutCall.js",
            // "should not interfere with local variables": "localVariables.js",
            // "should not interfere with local functions": "localFunctions.js",
            // "should not add a dependency if a parameter with the same name already exists": "parameterWithSameNameExists.js",
            "should work with an anonymous module": "anonymousDefine1.js",
            "should work with an anonymous module that has no dependencies": "anonymousDefine2.js",
            "should perform method renaming if parameters provided": "methodRenaming.js",
            "should work when define() uses an arrow function": "defineUsesArrowFunction.js",
            "should handle dependencies that were declared but not injected": "nonInjectedDependencies.js",
        };
        Object.keys(tests).forEach(description => {
            it(description, function() {
                return assertTransform(tests[description]);
            });
        });
    });

    describe("require call replacements", function() {
        const assertTransform = assertTransformFactory("codemods/inject-modules.js")

        it("should inject dependencies in to the require() call", function() {
            return assertTransform("allInOne.js");
        });
    });

    describe("property replacements", function() {
        const assertTransform = assertTransformFactory("codemods/inject-modules-with-instance-properties.js")

        it("should replace the module and leave the property call", function() {
            return assertTransform("propertyReplacement.js");
        });
    });

});
