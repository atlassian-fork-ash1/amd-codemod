var fs = require('fs');
var path = require('path');

var chai = require('chai');
chai.should();
chai.use(require('chai-things'));
chai.use(require('chai-as-promised'));
var assert = chai.assert;
var expect = chai.expect;

const core = require('jscodeshift/dist/core');
core.use(require('../lib/plugins/get-amd-calls'));

describe('AMD Call Methods:', function() {

    var inputFile = path.resolve(__dirname, 'input/allInOne.js');
    var inputSrc = fs.readFileSync(inputFile, 'utf8');
    var src = core(inputSrc);

    describe('getAMDDefineCalls', function() {
        it('returns all define() call expressions', function() {
            var defineCalls = src.getAMDDefineCalls();
            var nodes = defineCalls.nodes();

            assert.equal(nodes.length, 2);
            assert.equal(nodes[0].type, 'CallExpression');
            assert.equal(nodes[0].callee.name, 'define');
            assert.equal(nodes[0].arguments[0].value, 'module/a');
        });
    });

    describe('getAMDRequireCalls', function() {
        it('returns only top-level require() call expressions by default', function() {
            var requireCalls = src.getAMDRequireCalls();
            var nodes = requireCalls.nodes();

            assert.equal(nodes.length, 1);
            assert.equal(nodes[0].type, 'CallExpression');
            assert.equal(nodes[0].callee.name, 'require');
        });

        it('returns all require() calls when "true" is passed', function() {
            var requireCalls = src.getAMDRequireCalls(true);
            var nodes = requireCalls.nodes();

            assert.equal(nodes.length, 2);

            var r1 = core(nodes[0]).toSource();
            var r2 = core(nodes[1]).toSource();

            assert.equal(r2, 'require("module/a")');
        });
    });

    describe('getAMDCalls', function() {
        it('returns all top-level AMD call expressions', function() {
            var calls = src.getAMDCalls();
            var nodes = calls.nodes();

            assert.equal(nodes.length, 3);
            assert.equal(nodes[0].arguments[0].value, 'module/a');
            assert.equal(nodes[2].arguments[0].value, 'module/b');
        });
    });
});
