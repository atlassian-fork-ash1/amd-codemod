"use strict";

/**
 * Finds the first path that satisfies `fn`
 *
 * @param {Function} fn Function to run against each path. As first argument, it
 *                      will receive a collection containing one single path.
 * @return {Collection} Collection that contains the first path that satisfies
 *                      `fn`, or an empty collection.
 */
function findFirst(fn) {
    let idx = 0;

    while (idx < this.size()) {
        let candidate = this.at(idx);
        if (fn(candidate)) {
            return candidate;
        }
        idx++;
    }

    return new this.constructor([], this);
}

/**
 * Evaluates if the current collection has at least one path that satisfies
 * `fn`.
 *
 * This function is useful to do things like 'find all VariableDeclarators
 * which contain a Literal'. The naive approach does not work:
 *
 *      j(elements).find(j.VariableDeclarators).find(j.Literal)
 *
 * because it will return a collection of Literals, not a collection of
 * VariableDeclarators.
 *
 * With this method, we can do:
 *
 *     j(elements).find(j.VariableDeclarators).where(col => col.find(j.Literal))
 *
 *
 * @param {Function} fn Function to run against each path in the collection. It
 *                      should return a collection of the filtered elements. If
 *                      the returned collection is not empty, the original path
 *                      will be part of the filtered result. The function
 *                      receives a Collection with one single path.
 * @return {Collection} Subset of the original paths that satisfies `fn`.
 */
function where(fn) {
    return this.filter(path => fn(new this.constructor([path])).size() > 0);
}

/**
 * Simple method to detect if a Collection satisfies a particular function.
 *
 * @param {Function} fn Function to test against each item in the Collection.
 * @return {Boolean} True if the collection has at least one item that satisfies
 *                   `fn`. False otherwise.
 */
function has(fn) {
    return this.filter(fn).size() > 0;
}

module.exports = j => {
    j.registerMethods({
        findFirst,
        where,
        has
    });
}