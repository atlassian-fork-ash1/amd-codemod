const ImportUtil = require('../util/import-util');

/**
 * Replaces define([]) calls with es6 import/exports
 */
module.exports = function(file, api) {
    const j = api.jscodeshift;
    const root = j(file.source);
    const util = new ImportUtil(j);

    /**
     * Convert an `return` to `export default`.
     */
    function amdExportToEs6Export(body) {

        let exportStatement;

        const possibleReturn = body.filter(function (node) {
            return node.type === 'ReturnStatement'
        }).reduce(function (prev, cur) {
            return cur;
        }, null);

        const possibleExports = body.filter(function (node) {
            if (node.type === 'ExpressionStatement' &&
                node.expression.left &&
                node.expression.left.object &&
                node.expression.left.object.name === 'exports') {
                return node;
            }
        });

        /*
         * Handle default return case
         *
         * Before:
         * define([], () => {
         *      return 'some-value'
         * });
         *
         * After:
         * export default 'some-value'
         */
        if (possibleReturn && body.indexOf(possibleReturn) != -1) {
            exportStatement = j.exportDeclaration(true, possibleReturn.argument);
            body[body.indexOf(possibleReturn)] = exportStatement;

         /*
         * Handle exports case
         *
         * Before:
         * define(['exports'], (exports) => {
         *      exports.someProp = 'some-value';
         * });
         *
         * After:
         * export const someProp = 'some-value'
         */
        } else if (possibleExports) {
            possibleExports.forEach((node) => {
                exportStatement = j.exportDeclaration(false,
                    j.variableDeclaration('const', [
                        j.variableDeclarator(j.identifier(node.expression.left.property.name), node.expression.right)
                    ]
                ));
                body[body.indexOf(node)] = exportStatement;
            });

            const defaultProps = possibleExports.map((node) => {
                const namedExport = node.expression.left.property.name;
                const prop = j.property('init',
                    j.identifier(namedExport),
                    j.identifier(namedExport)
                );
                prop.shorthand = true;
                return prop;
            });

            body[body.length] = j.exportDeclaration(true, j.objectExpression(defaultProps));
        }

        return body;
    }

    /**
     * Once we remove the 'exports.' part of exports.something, then it could collide with other
     * declarions, so we prefix existing declarations with '_'
     */
    function handleExportNamingCollisions(astTree, exportDeclarations) {
        exportDeclarations.forEach((declaration) => {
            if (!declaration.default) {
                const exportName = declaration.declaration.declarations[0].id.name;
                astTree
                    .find(j.Identifier, {name: exportName})
                    .forEach((path) => {
                        // do not replace object properties
                        const parentNode = path.parent.value;
                        if (parentNode.type === 'MemberExpression') {
                            if (parentNode.object.name === exportName) {
                                path.get('name').replace(`_${exportName}`);
                            }
                        } else if (!/Property|MethodDefinition/.test(parentNode.type)) {
                            path.get('name').replace(`_${exportName}`);
                        }
                    });
                }
        });
    }

    function doDefineWithImports(p, importsArgument, closureArgument) {

        const props = importsArgument.elements;
        const comments = p.parent.value.comments || [];
        const importStatements = props
            .map((prop, i) => {
                 const variableName = closureArgument.params[i] && closureArgument.params[i].name;
                return {moduleName: prop.value, variableName}
            })
            .filter((descriptor) => descriptor.moduleName !== 'exports')
            .map((descriptor) => {
                return util.createImportStatement(descriptor.moduleName, descriptor.variableName);
            });


        // add the body after the import statements
        Array.prototype.push.apply(importStatements, closureArgument.body.body);

        // add any comments at the top
        importStatements[0].comments = comments;

        // this is the previous function body nodes replaced with the es6 export nodes
        const withExports = amdExportToEs6Export(importStatements);

        // jscodeshift fails to recognise function body if you use arrow function without braces,
        // e.g define(() => 'somereturn')
        if (!closureArgument.body.body) {
            throw new Error('It appears this module has no function body');
        }

        const codeAstTree = j(closureArgument.body.body);
        const exportDeclarations = withExports.filter((node) => node.type === 'ExportDeclaration');

        handleExportNamingCollisions(codeAstTree, exportDeclarations);

        j(p.parent).replaceWith(withExports);
    }

    root
        .find(j.CallExpression, { callee: { name: 'define' } }) // find define() function calls
        .filter((p) => p.parentPath.parentPath.name === 'body')
        .forEach((p) => {
            // define(function() { });
            if (p.value.arguments.length === 1) {
                // convert `return` statement to `export default`
                return j(p.parent).replaceWith(amdExportToEs6Export(p.value.arguments[0].body.body));
            }

            // define(['a', 'b', 'c'], function(a, b, c) { });
            if (p.value.arguments.length === 2) {
                return doDefineWithImports(p, p.value.arguments[0], p.value.arguments[1]);
            }

            // define('servicedesk/somemodule', ['a', 'b', 'c'], function(a, b, c) { });
            if (p.value.arguments.length === 3) {
                return doDefineWithImports(p, p.value.arguments[1], p.value.arguments[2]);
            }
        });

    const firstNode = root.find(j.Program).get('body', 0);

    root
        .find(j.CallExpression, { callee: { name: 'require' } }) // find define() function calls
        .forEach((p) => {
            
            const module = p.value.arguments[0].value;
            const moduleShortHand = module.split('/').pop();
            const moduleReferenceVar = moduleShortHand.replace(/-(\w{1})/g, function (str, p1) {
                return `${p1.toUpperCase()}`;
            });
            
            /**
             *  Check and replace webpack lazy loaded requires with es6 style ones (supported in webpack 2)
             * 
             *  replace:
             *  
             *  require.ensure([], () => {
             *      const someModule = require('some/module');
             *      ...
             *  });
             * 
             *  with:
             * 
             * System.import('some/module').then(someName => {
             *     const someModule= someName.default;
             *     ...
             * });
             */ 
            let current = p.parent;
            while (current.value.type !== 'Program'){
                // do stuff with node
                current = current.parent;
                const node = current.value;
                const callee = node.callee;
                if (node.type === 'CallExpression' &&
                    callee.object &&
                    callee.object.name === 'require' && 
                    callee.property &&
                    callee.property.name === 'ensure'
                    ) {
                        const systemImport = j.callExpression(
                            j.memberExpression(j.identifier('System'), j.identifier('import')),
                            [j.literal(module)]
                        );
                        const body = node.arguments[1].body;
                        const callback = j.arrowFunctionExpression([j.identifier(moduleReferenceVar)], body, false);
                        const replacementSatement = j.memberExpression(systemImport, j.callExpression(j.identifier('then'), [callback]));
                        
                        j(current).replaceWith(replacementSatement);
                        j(p).replaceWith(j.memberExpression(j.identifier(moduleReferenceVar), j.identifier('default')));
                        
                        return;
                    }                    
            }

            const importStatement = util.createImportStatement(module, moduleReferenceVar);
            
            j(firstNode).insertBefore(importStatement);

            // Replace the require('some/module').doSomething() & var something = require('some/module') 
            // the var assigned at import
            if (/VariableDeclarator|MemberExpression/.test(p.parent.value.type)) {
                p.replace(j.identifier(moduleReferenceVar));
            // otherwise remove the reference all together
            } else {
                p.prune();
            } 
        });

    return root.toSource({ quote: 'double' })
        .replace(/exports\./, '')
};
